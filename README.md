Minesweeper Clone
======================================

### STATUS:
* master:  TODO: CI Integration.
* develop:  TODO: CI Integration.

======================================
### Table of Contents
1. [Usage](https://gitlab.com/jeremymreed/mines#usage)
2. [Instructions](https://gitlab.com/jeremymreed/mines#usage)
3. [License](https://gitlab.com/jeremymreed/mines#license)


# Usage:
Assuming that you already have a C++ development environment
set up:

To build this program, you need to have make and a C++ compiler
installed.
```
make
```

To run the resulting binary (assuming the build succeeded):
```
./mines
```

The build process will leave some unneeded files, to clean up:
```
make clean
```
This will remove the binary, as well as object files generated
by the build process.

# Instructions:
When you start the program, you'll be shown the main menu.
The menu will give you options, allowing you to choose the
difficulty level, and an option to quit the game.

The main menu options are:
1. E: Start a game at the easy difficulty.
2. M: Start a game at the medium difficulty.
3. H: Start a game at the hard difficulty.
4. C: Start a game with a custom difficulty.
5. Q: Quit the game.

There are three default difficulty settings:
1. Easy:    10x10 tile board, 10 mines.
2. Medium:  30x20 tile board, 40 mines.
3. Hard:    40x40 tile board, 200 mines.

After selecting a difficulty level, the game starts, and
you'll see the game menu.  The game menu options are:
1. [P: Print the puzzle layer](https://gitlab.com/jeremymreed/mines#print-the-puzzle-layer)
2. [F: Place or clear a flag](https://gitlab.com/jeremymreed/mines#place-or-clear-a-flag)
3. [C: Select a square to uncover](https://gitlab.com/jeremymreed/mines#select-a-square-to-uncover)
4. [Q: Quit the game](https://gitlab.com/jeremymreed/mines#quit-the-game)

# Print the puzzle layer:
This is a debug function that allows you to view the
puzzle layer without the mask.  There is a mask layer that
determines what the player can see.  This layer also tells
the game if a square has been flagged or not.

# Place or clear a flag:
Selecting this option will cause the game to prompt you for
a x coordinate, then a y coordinate for a square.  If the
selected square is a valid coordinate, then the game will
place/clear a flag in that square, depending on if there
was a flag there previously.

# Select a square to uncover:
Selecting this option will cause the game to prompt you for
a x coordinate, then a y coordinate for a square.  If the
selected square is already uncovered, nothing happens.

But if the square is covered, the game will check to see
if a flag has been placed.  If a flag has been placed, the
game will do nothing, you'll need to clear the flag before you
can uncover the square.

If there is no flag, the game will uncover the square, and if
there is a mine there, your game is over!

If there is no mine, lucky you!

# Quit the game:
This ends the current game, and returns you to the main menu.

# License:
This software is licensed under the MIT License.
