// File:   dice.cxx
// Author: Jeremy Reed
// Contributors: NONE
// Purpose: Simulates rolling a n-sided die.

// Copyright © 2017 Jeremy M. Reed
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the “Software”), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:

// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.


#include <iostream>
#include "dice.h"

using namespace std;


namespace DICE
{


/////////////////////////////////////////////////////////////////////


   dice::dice()
   {
      cout << "dice::dice() called." << endl << endl;
      srand( time( 0 ) );
      result = 0;
   }


/////////////////////////////////////////////////////////////////////


   dice::~dice()
   {
      cout << "dice::~dice() called." << endl << endl;
   }

   int
   dice::roll( int N )
   {

      if( N > 0 )
      {
         result = 1 + ( rand() % N );
      }
      else
      {
         cout << "N cannot be 0 or negative!" << endl << endl;
         result = 0;
      }
      return( result );
   }

   int
   dice::roll( int N, int X )
   {
      result = 0;

      if ( N > 0 )
      {
         for ( int i = 0 ; i != X ; i++ )
         {
            result = result + roll( N );
         }
      }
      else
      {
         cout << "N cannot be 0 or negative!" << endl << endl;
         result = 0;
      }
      return( result ); 
   }

   int
   dice::roll_range( int start, int end )
   {
      start = start - 1;

      if ( ( start > 0 ) && ( end > 0 ) && ( start < end ) )
      {
         result = start + roll( end - start );
      }
      else
      {
         cout << "Both start and end cannot be 0 or negative!" << endl << endl;
         result = 0;
      }
      return result;
   } 

/////////////////////////////////////////////////////////////////////


}
