//File:   dice.h
//Author: Jeremy Reed
//Date:   5-26-2008
//Contributors: NONE
//Purpose: rolls an n-sided die.  The parameter n is given as input.

// Copyright © 2017 Jeremy M. Reed
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the “Software”), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:

// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.


#if !defined __DICE_H
#define __DICE_H

#include <iostream>
#include <stdlib.h>
#include <time.h>

namespace DICE
{
   class dice
   {

      public:  // Typedefs and public constants.


      private: // Data members

         int n;
         int result;

      public:  // Constructors and destructors

         dice();
         ~dice();

      private: // Special functions.

      public:  // Member functions

         int roll( int N );
         int roll( int N, int X);  // rolls a N sided die X times, returns
                                   // the sum. 
         int roll_range( int start, int end );  // Random number in the range of
                                                // [ start ... end ]

   }; // End class
}

#endif
