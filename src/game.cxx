// File:   game.h
// Author: Jeremy Reed
// Date:   5-26-2008
// Contributors: NONE
// Purpose: This Game class executes the game.

// Copyright © 2017 Jeremy M. Reed
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this->software and associated documentation
// files (the “Software”), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:

// The above copyright notice and this->permission notice shall be
// included in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.

#include <iostream>
#include <string.h>   // Provides C - style string functions.
#include <cctype>     // Provides toupper.
#include <iomanip>    // Provides setw to set the width of an output.
#include "game.h"
#include "dice.h"     // Provides the dice class.


// Constant values for Easy, Medium, and Hard puzzles.
#define E_ROW 10 
#define E_COL 10
#define E_N 10
#define M_ROW 30
#define M_COL 20
#define M_N 40
#define H_ROW 40
#define H_COL 40
#define H_N 200

using namespace std;

namespace GAME
{
  //-----------------------------------------------------------------------------

  game::game()
  {

  }

  //-----------------------------------------------------------------------------

  //-----------------------------------------------------------------------------
  
  game::~game()
  {

  }

  //-----------------------------------------------------------------------------

  //-----------------------------------------------------------------------------

  int
  game::clear_square( int** data, int** mask, int row, int col, int x, int y )
  {

     int** visited = NULL;  // Keep track of where we've already been.

     cout << "clear_square(...) called." << endl;

     cout << "x: " << x << ", y: " << y << endl;

     // Bounds check.
     if ( ( x < 0 ) || ( y < 0 ) || ( y >= row ) || ( x >= col ) )
     {
        return -1;
     }

     cout << "mask[y][x]: " << mask[y][x] << endl;

     if ( mask[y][x] == 1 )
     {
        // The square has already been cleared.
        return 0;
     }
     else if ( mask[y][x] == -1 )
     {
        // Shouldn't be able to clear a square that is already flagged.
        // The user should clear the flag first before attempting to clear
        // the square.
        return 0;
     }
     else
     {
        if ( data[y][x] == -1 )
        {
           // Oops, just hit a mine!
           return 1;
        }
        else if ( data[y][x] > 0 )
        {
           mask[y][x] = 1;
        }
        else
        {
           // Now we allocate memory to the visited array.
           this->make_dynamic_array( visited, row, col );

           // Clear the square.
           mask[y][x] = 1;
           visited[y][x] = 1;

           // Now we need to clear the neighbors!

           // Spawn calls in all 8 directions!
           this->clear_neighbor( data, mask, visited, row, col, (x-1), y );
           this->clear_neighbor( data, mask, visited, row, col, x, (y-1) );
           this->clear_neighbor( data, mask, visited, row, col, (x+1), y );
           this->clear_neighbor( data, mask, visited, row, col, x, (y+1) );
           this->clear_neighbor( data, mask, visited, row, col, (x-1), (y-1) );
           this->clear_neighbor( data, mask, visited, row, col, (x-1), (y+1) );
           this->clear_neighbor( data, mask, visited, row, col, (x+1), (y-1) );
           this->clear_neighbor( data, mask, visited, row, col, (x+1), (y+1) );

           // Deallocate memory reserved for the visited array.
           this->delete_dynamic_array( visited, row );

           cout << "Done!" << endl;

        }
     }

     return 0;
  }

  //-----------------------------------------------------------------------------


  //-----------------------------------------------------------------------------

  int
  game::clear_neighbor( int** puzzle, int** mask, int** viewed, int row, int col,
                      int x, int y )
  {

     // Bounds check.
     // Bounds check.
     if ( ( x < 0 ) || ( y < 0 ) || ( y >= row ) || ( x >= col ) )
     {
        return -1;
     }

     // Base case
     if ( ( puzzle[y][x] != 0 ) || ( viewed[y][x] != 0 ) )
     {
        if ( viewed[y][x] == 0 )
        {
           if ( puzzle[y][x] != -1 )
           {
              mask[y][x] = 1;
              viewed[y][x] = 1;
           }
        }
     }
     else
     {
        // Recursive case.
        mask[y][x] = 1;
        viewed[y][x] = 1;
        this->clear_neighbor( puzzle, mask, viewed, row, col, (x-1), y );
        this->clear_neighbor( puzzle, mask, viewed, row, col, x, (y-1) );
        this->clear_neighbor( puzzle, mask, viewed, row, col, (x+1), y );
        this->clear_neighbor( puzzle, mask, viewed, row, col, x, (y+1) );
        this->clear_neighbor( puzzle, mask, viewed, row, col, (x-1), (y-1) );
        this->clear_neighbor( puzzle, mask, viewed, row, col, (x-1), (y+1) );
        this->clear_neighbor( puzzle, mask, viewed, row, col, (x+1), (y-1) );
        this->clear_neighbor( puzzle, mask, viewed, row, col, (x+1), (y+1) );
     }

     return 0;
  }

  //-----------------------------------------------------------------------------


  //-----------------------------------------------------------------------------
  void
  game::delete_dynamic_array( int**& data, int row )
  {
     cout << "void delete_dynamic_array( ... ) called." << endl;
     int i;

     for( i = 0 ; i < row ; ++i )
     {
        delete [ ] data[i];
     }

     delete [] data;
     data = NULL;
  }
  //-----------------------------------------------------------------------------


  //-----------------------------------------------------------------------------

  int
  game::game_win( int** data, int** mask, int row, int col, int n )
  {

     int z = 0;

     for ( int i = 0 ; i != row ; i++ )
     {
        for ( int j = 0 ; j != col ; j++ )
        {
           if ( data[i][j] == -1 )
           {
              if ( mask[i][j] != -1 )
              {
                 return 0;
              }
              else
              {
                 z++;
              }
           }
        }
     }

     if ( z != n )
     {
        return 0;
     }
     else
     {
        return 1;
     }
  }

  //-----------------------------------------------------------------------------


  //-----------------------------------------------------------------------------

  int
  game::place_flag( int** puzzle, int** mask, int row, int col, int x, int y,
                  int& flagged )
  {

     cout << "row: " << row << endl
          << "col: " << col << endl
          << "x:   " << x << endl
          << "y:   " << y << endl << endl;

     if ( ( x < 0 ) || ( y < 0 ) || ( y >= row ) || ( x >= col ) )
     {
        return -1;
     }

     if ( mask[y][x] == -1 )
     {
        mask[y][x] = 0;
        flagged--;
     }
     else if ( mask[y][x] == 1 )
     {
        return 1;
     }
     else
     {
        mask[y][x] = -1;
        flagged++;
     }

     return 0;
  }

  //-----------------------------------------------------------------------------


  //-----------------------------------------------------------------------------

  int
  game::place_mines( dice* die, int**& data, int row, int col, int n )
  {

     cout << "place_mines called!" << endl;

     int x = 0;
     int y = 0;
     double z = ( (double) n / (double) ( row*col ) );
     int r = 0;

     if ( n > (row*col) )
     {
        return -1; 
     }

     // Check sparseness of minefield.
     cout << "Percentage mined: " << setprecision( 3 ) << z  << " %" << endl;
    
     //if ( z < 0.5 )
     if ( true )
     {
        // Initialize data to 0, randomly insert mines.
        for ( int i = 0 ; i != n ; i++ )
        {
           do
           {
              x = ( die->roll( col ) ) - 1;
              y = ( die->roll( row ) ) - 1;
              r = r + 1;
           }
           while( (int) data[y][x] == -1 );

           data[y][x] = -1;

           if ( y != ( row - 1 ) && ( data[y+1][x] != -1 ) )  
           {
              (data[y+1][x])++;
           }

           if ( y != 0  && ( data[y-1][x] != -1 ) )
           {
              (data[y-1][x])++;
           }

           if ( x != ( col - 1 )  && ( data[y][x+1] != -1 ) )
           {
              (data[y][x+1])++;
           }

           if ( x != 0  && ( data[y][x-1] != -1 ) )
           {
              (data[y][x-1])++;
           }

           if ( ( y != ( row - 1 ) ) && ( x != ( col - 1 ) )  
                && ( data[y+1][x+1] != -1 ) )
           {
              (data[y+1][x+1])++;
           }
   
           if ( ( y != ( row -1 ) ) && ( x != 0 )  && ( data[y+1][x-1] != -1 ) )
           {
              (data[y+1][x-1])++;
           }

           if ( ( y != 0 ) && ( x != ( col -1 ) )  && ( data[y-1][x+1] != -1 ) )
           {
              (data[y-1][x+1])++;
           }

           if ( ( y != 0 ) && ( x != 0 )  && ( data[y-1][x-1] != -1 ) )
           {
              (data[y-1][x-1])++;
           } 
        }
     } 
     else
     {
        // Initialize data to -1, randomly insert spaces.
        for ( int i = 0 ; i != row ; i++ )
        {
           for ( int j = 0 ; j != col ; j++ )
           {
              data[i][j] = -1;
           }
        }

        for ( int i = 0 ; i != n ; i++ )
        {
           do
           {
              x = ( die->roll( col ) ) - 1;
              y = ( die->roll( row ) ) - 1;
              r = r + 1;
           }
           while( (int) data[y][x] == 0 );

           data[y][x] = 0;

           if ( y != ( row - 1 ) && ( data[y+1][x] != -1 ) )
           {
              (data[y+1][x])++;
           }

           if ( y != 0  && ( data[y-1][x] != -1 ) )
           {
              (data[y-1][x])++;
           }

           if ( x != ( col - 1 )  && ( data[y][x+1] != -1 ) )
           {
              (data[y][x+1])++;
           }

           if ( x != 0  && ( data[y][x-1] != -1 ) )
           {
              (data[y][x-1])++;
           }

           if ( ( y != ( row - 1 ) ) && ( x != ( col - 1 ) )
                && ( data[y+1][x+1] != -1 ) )
           {
              (data[y+1][x+1])++;
           }

           if ( ( y != ( row -1 ) ) && ( x != 0 )  && ( data[y+1][x-1] != -1 ) )
           {
              (data[y+1][x-1])--;
           }

           if ( ( y != 0 ) && ( x != ( col -1 ) )  && ( data[y-1][x+1] != -1 ) )
           {
              (data[y-1][x+1])--;
           }

           if ( ( y != 0 ) && ( x != 0 )  && ( data[y-1][x-1] != -1 ) )
           {
              (data[y-1][x-1])--;
           }

        }
     }

     cout << "It took " << r << " attempts to place " << n << " mines." << endl;
     cout << "Done placing mines!" << endl;

     return 0;
  } 

  //-----------------------------------------------------------------------------


  //-----------------------------------------------------------------------------

  int
  game::play_game( int**& data, int**& mask, int row, int col, int n )
  {

      int retval = 0;
      int input1 = 0;
      int input2 = 0;
      int flagged = 0;
      char choice;              // A command character entered by the user

      do
      {

          if ( this->game_win( data, mask, row, col, n ) )
          {
             cout << "You've found all the mines!" << endl;
             return 0;
          }
          else
          { 

          cout << endl;
          cout << "You have " << ( n - flagged ) << " flags left to place."
               << endl;
          this->print_game( data, mask, row, col );
          this->print_game_menu( );
          choice = toupper(get_user_command( ));
          switch (choice)
          {
             
             case 'P':
                cout << "Printing the puzzle layer!" << endl << endl;
                this->print_puzzle( data, row, col );
                break;
             case 'F':
                cout << "Placing/Clearing a flag!" << endl;
                cout << "Give me a square!" << endl;
                cout << "x: ";
                input1 = this->get_int( );
                cout << "y: ";
                input2 = this->get_int( );
                while( ( retval = this->place_flag( data, mask, row, col, input1, input2,
                                              flagged ) ) )
                {
   
                   if ( retval != 1 )
                   {
                      cout << "retval: " << retval << endl;
                      cout << "Bad input!  Try again!" << endl;
                      cout << "x: ";
                      input1 = this->get_int( );
                      cout << "y: ";
                      input2 = this->get_int( );
                   }
                   else
                   {
                      cout << "You cannot place a flag here!" << endl;
          break;
                   }
                }
                break;
             case 'C':
                cout << "Clearing a square!" << endl;
                cout << "Give me a square!" << endl;
                cout << "x: ";
                input1 = this->get_int( );
                cout << "y: ";
                input2 = this->get_int( );
                while( ( retval = this->clear_square( data, mask, row, col, 
                                                input1, input2 ) ) != 0 )
                {
                   if ( retval != 1 )
                   {
                      cout << "Bad input!  Try again!" << endl;
                      cout << "x: ";
                      input1 = this->get_int( );
                      cout << "y: ";
                      input2 = this->get_int( );
                   }
                   else
                   {
                      cout << endl << endl << "Boom!  You hit a mine!" << endl
                           << "GAME OVER!!!" << endl << endl;
                      this->print_puzzle( data, row, col );
                      return 1;
                   }
                }
                break;
             case 'Q':
                cout << "Returning to main menu." << endl;
                break;
             default:
                cout << choice << " is invalid. Sorry." << endl;
                break;
          }
        }
      }
      while (choice != 'Q');

     return 0;
  }

  //-----------------------------------------------------------------------------


  //-----------------------------------------------------------------------------
  void
  game::print_menu( )
  {
      cout << endl;
      cout << "The following choices are available: " << endl;
      cout << " E  Easy difficulty." << endl;
      cout << " M  Medium difficulty." << endl;
      cout << " H  Hard difficulty." << endl;
      cout << " C  Custom difficulty." << endl;
      cout << " Q  Quit this->test program" << endl;
  }
  //-----------------------------------------------------------------------------


  //-----------------------------------------------------------------------------

  void
  game::print_game_menu( )
  {
     cout << endl;
     cout << "Player, choose your action!" << endl;
     cout << " P  Print Puzzle layer." << endl;
     cout << " F  Place/Clear a flag." << endl;
     cout << " C  Select a square to uncover." << endl;
     cout << " Q  Quit the game!" << endl;
  }

  //-----------------------------------------------------------------------------


  //-----------------------------------------------------------------------------

  void
  game::print_game( int** data, int** mask, int row, int col )
  {
     for ( int i = 0 ; i != row ; i++ )
     {
        for ( int j = 0 ; j != col ; j++ )
        {
           if ( mask[i][j] == 1 )
           {
              if ( data[i][j] > 0 )
              {
                 cout << data[i][j];
              }
              else if ( data[i][j] == 0 )
              {
                 cout << ".";
              }
              else
              {
                 cout << "m";
              }
           }
           else if ( mask[i][j] == -1 )
           {
              cout << "f";
           }
           else
           {
              cout << "#";
           }
        }
        cout << endl;
     } 
  }

  //-----------------------------------------------------------------------------


  //-----------------------------------------------------------------------------

  void
  game::print_puzzle( int** data, int row, int col )
  {
     for ( int i = 0 ; i != row ; i++ )
     {
        for ( int j = 0 ; j != col ; j++ )
        {
           if ( data[i][j] > 0 )
           {
              cout << data[i][j];
           }
           else if ( data[i][j] == 0 )
           {
              cout << ".";
           }
           else
           {
              cout << "m";
           }
        }
        cout << endl;
     }
  }

  //-----------------------------------------------------------------------------


  //-----------------------------------------------------------------------------
  int
  game::get_int( )
  // Library facilties used: iostream.h
  {
      int result;

      cout << "Please enter an integer : ";
      cin  >> result;
      cout << result << " has been read." << endl;
      return result;
  }
  //-----------------------------------------------------------------------------


  //-----------------------------------------------------------------------------
  char
  game::get_user_command( )
  // Library facilties used: iostream.h
  {
      char command;

      cout << "Enter choice: ";
      cin >> command;

      return command;
  }
  //-----------------------------------------------------------------------------


  //-----------------------------------------------------------------------------
  double
  game::get_number( )
  // Library facilties used: iostream.h
  {
      double result;

      cout << "Please enter the next real number for the sequence: ";
      cin  >> result;
      cout << result << " has been read." << endl;
      return result;
  }
  //-----------------------------------------------------------------------------


  //-----------------------------------------------------------------------------
  unsigned int
  game::get_unsigned_int( )
  // Library facilties used: iostream.h
  {
      unsigned int result;

      cout << "Please enter an unsigned integer : ";
      cin  >> result;
      cout << result << " has been read." << endl;
      return result;
  }
  //-----------------------------------------------------------------------------

  int
  game::make_dynamic_array( int**& data, int row, int col )
  {
     int i,j;
                                     // Avoid using the global temp ptr.
     if ( row == 0 || col == 0 )
     {
        cout << "Cannot have one dimension equal to zero," << endl
             << "and have the other equal to a non-zero number. """
             << endl << endl;
        return -1;
     }
     else 
     {

        if ( row > 0 && col > 0 )
        {
           data = new int*[ row ];
           for ( i = 0 ; i < row ; i++ )
           {
              data[ i ] = new int[ col ];

           }

           for ( i = 0 ; i != row ; i++ )
           {
              for ( j = 0 ; j != col ; j++ )
              {
                 data[i][j] = 0;
              }
           }
        }
        else
        {
           cout << "Cannot have negative values for rows and columns!"
                << endl
                << "Doing nothing!"
                << endl << endl;
           return -1;
        }
     }
     return 0;
  }
  //-----------------------------------------------------------------------------


  //-----------------------------------------------------------------------------

  int
  game::run( )
  {
    dice die;          // Our random number generator.
    dice* die_ptr = &die;
    char choice = '0'; // A command character entered by the user
    int n = 0;    // Number of mines to use in this->puzzle.
    int x = 0;
    int y = 0;
    int** puzzle;    //  -1, if there is a mine.
                        //   0, if this->space is not adjacent to a mine.
                        // 1-9, if this->space is adjacent to 1-9 mines.
    int** mask;      // Determines if the location is uncovered.
                        // given location.


    do
    {
        cout << endl;
        this->print_menu( );
        choice = toupper(get_user_command( ));
        switch (choice)
        {
        case 'E':
            cout << "Playing an easy puzzle!" << endl;
            this->make_dynamic_array( puzzle, E_ROW, E_COL );
            this->make_dynamic_array( mask, E_ROW, E_COL );
            this->place_mines( die_ptr, puzzle, E_ROW, E_COL, E_N );
            this->play_game( puzzle, mask, E_ROW, E_COL, E_N );
            this->delete_dynamic_array( puzzle, E_ROW );
            this->delete_dynamic_array( mask, E_ROW );
            break;
        case 'M':
            cout << "Playing a medium puzzle!" << endl;
            this->make_dynamic_array( puzzle, M_ROW, M_COL );
            this->make_dynamic_array( mask, M_ROW, M_COL );
            this->place_mines( die_ptr, puzzle, M_ROW, M_COL, M_N );
            this->play_game( puzzle, mask, M_ROW, M_COL, M_N );
            this->delete_dynamic_array( puzzle, M_ROW );
            this->delete_dynamic_array( mask, M_ROW );
            break;
        case 'H':
            cout << "Playing a hard puzzle!" << endl;
            this->make_dynamic_array( puzzle, H_ROW, H_COL );
            this->make_dynamic_array( mask, H_ROW, H_COL );
            this->place_mines( die_ptr, puzzle, H_ROW, H_COL, H_N );
            this->play_game( puzzle, mask, H_ROW, H_COL, H_N );
            this->delete_dynamic_array( puzzle, H_ROW );
            this->delete_dynamic_array( mask, H_ROW );
            break;
        case 'C':
            cout << "Set up a custom puzzle." << endl;
            cout << "Number of columns: ";
            x = this->get_int( );
            cout << "Number of rows: ";
            y = this->get_int( );
            cout << "Number of mines: ";
            n = this->get_int( );

            while( ( x < 0 ) || ( y < 0 ) || ( n > ( y * x ) ) )
            {
               cout << "Bad input!  Try again!" << endl;
               cout << "Number of columns: ";
               x = this->get_int( );
               cout << "Number of rows: ";
               y = this->get_int( );
               cout << "Number of mines: ";
               n = this->get_int( );
            }

            this->make_dynamic_array( puzzle, y, x );
            this->make_dynamic_array( mask, y, x );
            this->place_mines( die_ptr, puzzle, y, x, n );
            this->play_game( puzzle, mask, y, x, n );
            this->delete_dynamic_array( puzzle, y );
            this->delete_dynamic_array( mask, y );

            break;
        case 'Q':
            cout << "Goodbye!" << endl;
            break;
        default:
            cout << choice << " is invalid. Sorry." << endl;
            break;
        }
    }
    while (choice != 'Q');

    return 0;
  }

  //-----------------------------------------------------------------------------
}
