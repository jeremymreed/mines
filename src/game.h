// File:   game.h
// Author: Jeremy Reed
// Date:   5-26-2008
// Contributors: NONE
// Purpose: This Game class executes the game.

// Copyright © 2017 Jeremy M. Reed
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the “Software”), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:

// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.

#if !defined __GAME_H
#define __GAME_H

#include <iostream>
#include "dice.h"

using namespace DICE;

namespace GAME
{
  class game
  {

    public:  // Typedefs and public constants.


    private: // Data members

    public:  // Constructors and destructors

      game();
      ~game();

    private: // Special functions.

    public:  // Member functions

      int clear_square( int** data, int** mask, int row, int col, int x, int y );
      // Preconditions: row and col must be positive.  x and y must be postive, 
      //                y < row, and x < col.
      // Postcondition: The requested square has been cleared.  
      //                There are three possible return values.
      //                This function returns -1 if the input is invalid.
      //                This function returns 0 if there was no error.
      //                This function returns 1 if a mine has been uncovered.
      //                If the square has already been cleared, this function
      //                does nothing, and returns 0.

      int clear_neighbor( int** data, int** mask, int** viewed, int row, int col, 
                          int x, int y );
      // Preconditions: row and col must be positive.  x and y must be positive,
      //                y < row, and x < col.
      //                Values for dir: No diagonals!
      //                0: up.
      //                1: left.
      //                2: down.
      //                3: right.
      //
      // Postconditions: Squares have been cleared in the given direction, stopping
      //                 only when we reach a square holding a postive, non-zero 
      //                 value.
      //                 This function will be a recursive function,
      //                 with a stopping case being a square w/ a postive, non-zero
      //                 value.

      void delete_dynamic_array( int**& data, int row );
      // Postcondition: Frees up dynamic memory.

      int game_win( int** data, int** mask, int row, int col, int n );

      int place_flag( int** data, int** mask, int row, int col, int x, int y,
                      int& flagged );
      // Preconditions: row and col must be positive.  x and y must be postive, 
      //                y < row, and x < col.
      // Postcondition: The requested square has been cleared.  
      //                There are three possible return values.
      //                This function returns -1 if the input is invalid.
      //                This function returns 0 if there was no error.
      //                This function returns 1 if the square has already been 
      //                flagged.

      int place_mines( dice* die, int**& data, int row, int col, int n );
      // Preconditions: row and col must be positive.  n must be less than or equal
      //                to ( row * col ).  Otherwise we return -1, and data remains
      //                unchanged.
      // Postconditions: Return values are described in the precondition.  If there
      //                 is no error, data is modifified to contain mines,
      //                 and all locations adjacent to mines have a numerical value
      //                 indicating how many mines are adjacent.

      int play_game( int**& data, int**& mask, int row, int col, int n );
      // Preconditions: both data, and mask are valid arrays.  row and col are
      //                positive.
      // Postconditions: A game of minesweeper has been played.
      //                 The return value indicates the outcome of the game.
      //                 TODO: Should change this return value to include
      //                 the length of time it took to play the game.
      //                 This feature isn't so important for a text-based version,
      //                 but will be needed for a graphical version.

      void print_menu( );
      // Postcondition: A menu of choices for this program has been written to cout.
      // Library facilties used: iostream.h

      void print_game_menu( );
      // Postcondition: A menu has been printed to the screen.  This is a sub-menu.

      void print_game( int** data, int** mask, int row, int col );
      // Postcondition: The game's state has been printed to the screen.
      //                This function uses the mask to determinate what can be
      //                shown to the player.

      void print_puzzle( int** data, int row, int col );
      // Postcondition: This function prints the puzzle to the screen.
      //                The mask is not used at all, so all information
      //                may be viewed by the player.

      int get_int( );
      // Postcondition: An integer has been read in from input.

      char get_user_command( );
      // Postcondition: The user has been prompted to enter a one character command.
      // A line of input (with at least one character) has been read, and the first
      // character of the input line is returned.

      double get_number( );
      // Postcondition: The user has been prompted to enter a real number. The
      // number has been read, echoed to the screen, and returned by the function.

      unsigned int get_unsigned_int( );
      // Postcondition: An unsigned int has been read in from input.

      int make_dynamic_array( int**& data, int row, int col );

      int run( );
  }; // End class
}

#endif